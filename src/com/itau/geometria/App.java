package com.itau.geometria;

import java.util.List;

import com.itau.geometria.console.Console;
import com.itau.geometria.construtores.ConstrutorFormas;
import com.itau.geometria.excecoes.NaoImplementadoException;
import com.itau.geometria.excecoes.TrianguloInvalidoException;
import com.itau.geometria.formas.Circulo;
import com.itau.geometria.formas.Forma;
import com.itau.geometria.formas.Retangulo;
import com.itau.geometria.formas.Triangulo;

public class App {
	public static void main(String[] args) throws TrianguloInvalidoException, NaoImplementadoException {
		List<Double> lista = Console.lerEntrada();
		Forma forma = ConstrutorFormas.construir(lista);
		
		Console.imprimir(forma);
	}

}
